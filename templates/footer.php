<section id="footer_widget_content" class="" style="padding-top: 90px; padding-bottom: 90px; background-color: rgb(46, 46, 46);">
	<div class="container">
		<div class="row">
	    		<?php dynamic_sidebar('sidebar-footer'); ?>
		</div>
	</div>
</section>
<footer class="content-info">
	<p class="text-xs-center">
		Copyright (с) 2016  <a href="#" rel="nofollow"> Hummingbird Web Solutions Pvt Ltd </a>All Rights Reserved. 
	</p>

</footer>


<section class="home_page_banner">
	   <div class="container">
            <div class="row">
                <div class="col-md-10 push-md-1 text-xs-center">
                    <h1 class="title">WordPress Auction Software Solution</h1>
                    <p class="sub-title">Turn any WordPress site into a professional auction website with WP Auction Software plugin<br></p>
                    <div class="">
			<a class="btn btn-lg btn-primary" href="#">
				<span class="socicon socicon-windows sage-iconfont sage-iconfont-btn"></span>
				Buy Now
			</a>
		    </div>
                </div>
            </div>
        </div>
</section>
<div class="container">
	<?php the_content(); ?>
</div>
<?php //wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>

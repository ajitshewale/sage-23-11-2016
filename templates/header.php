<header id="ext_menu-0" class="banner">

    <nav class="navbar navbar-dropdown  navbar-fixed-top navbar-default">
      <div class="container">
    <div class="sage-table">
                <div class="sage-table-cell">

                    <div class="navbar-brand">
			 <a href="#" class="navbar-logo navbar-caption">
				<img src="https://numixproject.org/res/img/numix-logo.png" alt="logo">
			</a>
                        <a class="navbar-caption" href="<?= esc_url(home_url('/')); ?>"><?php bloginfo('name'); ?></a>
                    </div>

                </div>
                <div class="sage-table-cell">

                    <button class="navbar-toggler pull-xs-right hidden-md-up" type="button" data-toggle="collapse" data-target="#nav-content">&#9776;
                    </button>
  
    <?php
	      if (has_nav_menu('primary_navigation')) :
	      wp_nav_menu( array(
	      		'menu'              => 'primary_navigation',
	      		'theme_location'    => 'primary_navigation',
			'depth'		    => '1',
			'container' 	    => '',
			'container_class'   => 'menu-header',		
			'menu_id'     	    => 'nav-content',
	      		'menu_class'        => 'nav-dropdown collapse pull-xs-right nav navbar-nav navbar-toggleable-sm',
	      		'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
	      		'walker'            => new wp_bootstrap_navwalker())
	      		);
	      endif;
	      ?>  	
		<button hidden="" class="navbar-toggler navbar-close" type="button" data-toggle="collapse" data-target="#nav-content">
                         &#9776;
                </button>
	</div>
      </div>
  </div>	
</nav>
</header>
